import csv
import os.path
import sys
from dateutil import parser
from datetime import datetime, timedelta
import calendar
import time
import re
from dateutil import parser
import texttable as tt


CASH_BALANCE_END = 53.46 # balance of cash within funding circle at the end of the transactions.
INVESTMENT_BALANCE_START=4116.72 # estimate or actual amount of investment at start of the transactions


LOAN_PARTS_PATHNAME="e:\\dropbox\\p2p\\accounts\\loan_parts_29Nov.csv"
TRANSACTIONS_PATHNAME="E:\Dropbox\p2p\\accounts\\all.csv"
DATE_START=parser.parse("01/01/2017",  dayfirst=True)



class Book():
    def __init__(self, loanId, lastRepaymentMonth):
        self.loanId = loanId

        self.lastActiveMonth = lastRepaymentMonth
        self.closingBalance = 0
        self.closingStatus = "UnknownPresumedSold"
        self.account = {}

        for month in range(1, 13):
            self.account[month] =  {'delta' : 0,
                        'rollingDelta' : 0,
                        'balance' : 0,
                        'dPlus': 0, 'dMinus' : 0,
                        'status' : 'unknown'}



lastMonth = 0 # last month in transactions file

loanBooks = {} # all loans seen in the transactions file
cashBook = Book("cash", 0) # cash - all transactions are either in or out of the cashbook
investmentBook = Book("invest", 0) # tracks payments made into FC from outside bank account


def diff_month(d1, d2):
    return (d1.year - d2.year) * 12 + d1.month - d2.month


def parseMonth(dateString):
    #print (dateString)
    dt = parser.parse(dateString,  dayfirst=True)
    return 1 + diff_month(dt, DATE_START)

def updateBook(books, month, loanId, amount):


    if isinstance(books, Book): # if cash book
        books.account[month]['delta'] += amount

        if amount > 0:
            books.account[month]['dPlus'] += amount
            books.lastActiveMonth = month
        else:
            books.account[month]['dMinus'] += amount
            books.lastActiveMonth = month


    else:
        books[loanId].account[month]['delta'] += amount

        if amount > 0:
            books[loanId].account[month]['dPlus'] += amount
            books[loanId].lastActiveMonth = month
        else:
            books[loanId].account[month]['dMinus'] += amount
            books[loanId].lastActiveMonth = month


# assume loan exists
def setLoanStatus(loanId, month, status):
    if loanId not in loanBooks:
        print("Loan does not exist")
        exit(1)

    loanBooks[loanId].account[month]['status'] = status


def transfer(fromBooks, toBooks, month, loanId, amount):

    if loanId not in loanBooks:  # if new loan
        loanBooks[loanId] = Book(loanId, month)
    # if amount == 0:
    #     print("Zero value transaction")
    #     exit(1)

    if fromBooks != None:
        updateBook(fromBooks, month, loanId, -amount)

    if toBooks != None:
        updateBook(toBooks, month, loanId, amount)



def processTransactions(f):
    global loanParts, lastMonth
    csv_reader = csv.reader(f)
    csv_writers = {}
    for row in csv_reader:
        if row[0] == 'Date':
            continue
        #print(row)

       # if row[1].startswith('Loan Part ID 20269309'):
       #     print("TEST")

        #parse month
        month = parseMonth(row[0])

        #set last month
        if month > lastMonth:
            lastMonth = month

        # parse amount
        if (row[2] == ''):
            amountIn = 0
        else:
            amountIn = float(row[2])

        if (row[3] == ''):
            amountOut = 0
        else:
            amountOut = float(row[3])

        # MAIN TRANSACTIONS PROCESSING
        if row[1].startswith('Loan offer'): # buying a loan from primary market
            loanId = parseLoanId(row[1])
            transfer(cashBook, loanBooks, month, loanId, amountOut)
            setLoanStatus(loanId, month, "purchased")

        elif row[1].startswith('Loan Part ID'): # buying or # selling a loan
            (loanPartId, principle, interest, delta, fee) = parseSellingLoan(row[1])

            loanId = getLoanId(loanPartId)
            if row[2] != '':
                if amountIn == fee:
                    transfer(None, cashBook, month, loanId, amountIn)  # fee
                elif amountIn == principle:
                    transfer(loanBooks, cashBook, month, loanId, amountIn)  # loan repaid
                elif amountIn == (interest + delta):
                    transfer(None, cashBook, month, loanId, amountIn)  # remaining interest repaid
            elif row[3] != '':
                if amountOut == fee:
                    transfer(cashBook, None, month, loanId, amountOut) # fee
                elif amountOut == principle:
                    transfer(cashBook, loanBooks, month, loanId, amountOut)  # loan repaid
                elif amountOut == (interest + delta):
                    transfer(cashBook, None, month, loanId, amountOut)  # remaining interest repaid
            else:
                print("Unexpected amountIn")


            setLoanStatus(loanId, month, "sold")

        elif row[1].startswith('Principal repayment') \
                or row[1].startswith('Early principal repayment') \
                or row[1].startswith('Principal recovery'):
            loanPartId = parseLoanPartId(row[1])
            loanId = getLoanId(loanPartId)
            transfer(loanBooks, cashBook, month, loanId, amountIn)
            setLoanStatus(loanId, month, "paid")

        elif row[1].startswith('Interest repayment') \
                or row[1].startswith('Early interest repayment') \
                or row[1].startswith('Interest recovery'):
            transfer(None, cashBook, month, loanId, amountIn)
            setLoanStatus(loanId, month, "paid")

        elif row[1].startswith('Servicing fee'):
            transfer(cashBook, None, month, loanId, amountOut)
            setLoanStatus(loanId, month, "paid")

        elif row[1].startswith('EPDQ'): # cash in
            transfer(None, cashBook, month, loanId, amountIn)
            transfer(None, investmentBook, month, loanId, amountIn)

        elif row[1].startswith('Reverse transaction'):
            loanPartId = parseLoanPartId(row[1])
            loanId = getLoanId(loanPartId)
            transfer(cashBook, loanBooks, month, loanId, amountOut)
            setLoanStatus(loanId, month, "reversed")
        else:
            print("Unknown transaction type: ", row[1])
            exit(1)




def parseSellingLoan(mystring):
    mymatch = re.match(".*Loan Part ID (\d+) : Principal (\d+.\d+), Interest (\d+.\d+), Delta (\d+.\d+), Fee (\d+.\d+)", mystring)
    if mymatch and mymatch.group(1):
        return (mymatch.group(1),
                float(mymatch.group(2)),
                float(mymatch.group(3)),
                float(mymatch.group(4)),
                float(mymatch.group(5)))


    print("Couldn't find loan Id inb loan sale")
    exit(1)
    return None


def parseLoanId(mystring):
    mymatch = re.match(".+ - (\d+)", mystring)
    if mymatch and mymatch.group(1):
        return mymatch.group(1)


    print("Couldn't find loan Id")
    exit(1)
    return None

def parseLoanPartId(mystring):

    mymatch = re.match(".*loan part (\d+)", mystring)
    if mymatch and mymatch.group(1):
        return mymatch.group(1)

    mymatch = re.match(".*Loan Part ID (\d+)", mystring)
    if mymatch and mymatch.group(1):
        return mymatch.group(1)

    exit(1)
    print("Couldn't find loan part Id")

    return None

def loadLoanParts(filename):
    loanParts = {}
    with open(filename, mode='r', newline='') as f:
        csv_reader = csv.reader(f)
        csv_writers = {}
        for row in csv_reader:
            if row[0] != 'Loan part ID':
               # print(row)
                loanParts[row[0]] = {'loanId':row[3], 'amount':float(row[6]), 'finalStatus':row[9]}


    return loanParts

def getLoanId(loanPartId):

    if loanPartId in loanParts:
        loanId = loanParts[loanPartId]['loanId']
    else:
        loanId = "P_" + loanPartId

    if (loanId == '229'):
        pass

    return loanId

global loanParts
def rollingAvg(prev, sample1, N1):
    return prev + (sample1 - prev) / N1

def calculateRollingDeltas(book, rollingKey, startValue = 0):
    for month in range(1, 13):
        if month == 1:
            book.account[month][rollingKey] = book.account[month]['delta'] + startValue
        else:
            book.account[month][rollingKey] = \
                round(book.account[month - 1][rollingKey] + book.account[month]['delta'], 2)

if __name__ == '__main__':
    global loanParts
    loanParts = loadLoanParts(LOAN_PARTS_PATHNAME)

    input_filename = TRANSACTIONS_PATHNAME

    # process transactions
    with open(input_filename, mode='r', newline='') as f:
        processTransactions(f)

    # calculate closing balances and final status from the loadParts spreadsheet
    for loanPartId in loanParts:
        loanId = getLoanId(loanPartId)

        if loanId in loanBooks:
            loanBooks[loanId].closingBalance += loanParts[loanPartId]['amount']
            loanBooks[loanId].closingStatus = loanParts[loanPartId]['finalStatus']

    # cash and investment book deltas
    calculateRollingDeltas(cashBook, 'rollingDelta')
    calculateRollingDeltas(investmentBook, 'rollingDelta')

    # cash and investment book balanvces
    cashBook.closingBalance = CASH_BALANCE_END
    cashBook.openingBalance = cashBook.closingBalance - cashBook.account[lastMonth]['rollingDelta']
   # cashBook.openingBalance = 2893.0
   # cashBook.closingBalance = cashBook.openingBalance + cashBook.account[lastMonth]['rollingDelta']

    calculateRollingDeltas(cashBook, 'balance', cashBook.openingBalance)
    calculateRollingDeltas(investmentBook, 'balance', INVESTMENT_BALANCE_START)

    # loan book deltas and balances
    for loanId in loanBooks:
        # calculate rolling delta
        calculateRollingDeltas(loanBooks[loanId], 'rollingDelta')


        # calculate opening balances
        loanBooks[loanId].openingBalance = loanBooks[loanId].closingBalance - loanBooks[loanId].account[lastMonth]['rollingDelta']

        # calculate monthly balances
        calculateRollingDeltas(loanBooks[loanId], 'balance', loanBooks[loanId].openingBalance)

        # check for default
        if loanBooks[loanId].lastActiveMonth < lastMonth:
            if (loanBooks[loanId].closingStatus == "Bad Debt"):
                print("Defaulting loan ", loanId, " in month ", loanBooks[loanId].lastActiveMonth+1, " balance: ", round(loanBooks[loanId].closingBalance,2))
                for defaultMonth in range(loanBooks[loanId].lastActiveMonth+1, (lastMonth+1)):
                    loanBooks[loanId].account[defaultMonth]['status'] = 'default'


    # RESULTS !
    table = tt.Texttable()
    table.header(['Month','Cash','Loans', 'SubTotal', ' ', 'Defaults', 'Profit'])
    table.set_precision(2)
    rollingMonthlyLoanBalance = 0
    for month in range(1, lastMonth+1):
        monthlyLoanBalance = 0
        monthlyDefaultBalance = 0
        for loanId in loanBooks:
            if loanBooks[loanId].account[month]['status'] != 'default':
                monthlyLoanBalance += loanBooks[loanId].account[month]['balance']
            else:
                monthlyDefaultBalance += loanBooks[loanId].account[month]['balance']

        total = monthlyLoanBalance + cashBook.account[month]['balance']
        subTotal = monthlyLoanBalance + cashBook.account[month]['balance'] + monthlyDefaultBalance
        profit = round(total - investmentBook.account[month]['balance'], 2)

        #calculate investment rolling avg
        # investmentRollingAvg = 0.0
        # for subMonth in range (1, month+1):
        #     investmentRollingAvg += investmentBook.account[subMonth]['balance']
        # investmentBook.account[month]['rollingAvg'] = investmentRollingAvg / month
        #

        rollingMonthlyLoanBalance = rollingAvg(rollingMonthlyLoanBalance, monthlyLoanBalance, month)
        monthlyDefaultPercent = round((1200.0 / month) * profit / rollingMonthlyLoanBalance,2  )



        table.add_row ([month,
                cashBook.account[month]['balance'],
                monthlyLoanBalance - monthlyDefaultBalance,
                subTotal,
                "",
                monthlyDefaultBalance,
                str(profit) + " (" + str(monthlyDefaultPercent) + '%)'
                        ])

    print(table.draw())
    exit(1)


