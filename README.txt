Use this software at your own risk. It is highly likely to contain bugs. It is provided free as a gesture to the community.
I take no responsibility for any damage or loss that may occur as the result of using this software.

This software reads every transaction in the FC transactions .csv file.
Each transaction represents a payement to or from the FC current account.
The software uses a cashbook and loans book to record each transaction.
For example, a new loan investment would involve a reduction of cash in the cashbook and an increase of cash for the corresponding loan in the loan book.
Transactions are all aggregated into the given month. Thus you can see what happened to the cashbook or loanbook from month to month.
Dedfaults are tracked by noticing when the first month no payment was received for a live loan in a given month.
If that is the case, the loan is checked within the loanparts.csv file for the status 'Bad Debt'.



A sample output for Jan-Nov this year looks like this.
Loans - Total amount of cash in loanbook (but not defaults)
SubTotal = Cash + Loans
Default - Cumulative amount of defaults.
Profit  - Cumulative Cash+Loans - Investments

So for example in April, I had earned a total of �86.35 profit which gives an apr of around 5.93% of the 
whole period (Jan-April).
Over the 11 months shown here it would seem to be I have made a total annualised rate of around 5.09%.
Note that this is currently calculated by looking at the total profit divided by the average amount invested per month.
It will be less accurate in situations like the last month (November in this example) has a signifcant withdrawl or deposit towards the end of that month.

SAMPLE_OUTPUT: 

D:\Python36-32\python.exe E:/p2p/accounts/fc.py
Defaulting loan  2970  in month  7  balence:  9.72
Defaulting loan  14824  in month  4  balence:  18.25
Defaulting loan  5997  in month  3  balence:  8.05
Defaulting loan  8167  in month  3  balence:  14.21
Defaulting loan  5702  in month  3  balence:  18.57
Defaulting loan  144  in month  11  balence:  11.79
Defaulting loan  277  in month  4  balence:  15.54
Defaulting loan  36931  in month  8  balence:  58.74
Defaulting loan  37958  in month  9  balence:  60.0
Defaulting loan  13433  in month  11  balence:  16.64
Defaulting loan  38331  in month  10  balence:  116.64
Defaulting loan  39532  in month  10  balence:  58.74
+-------+---------+----------+----------+---+----------+----------------+
| Month |  Cash   |  Loans   | SubTotal |   | Defaults |     Profit     |
+=======+=========+==========+==========+===+==========+================+
| 1     | 21.66   | 4095.06  | 4116.72  |   | 0        | 0.0 (0.0%)     |
+-------+---------+----------+----------+---+----------+----------------+
| 2     | 71.33   | 4097.10  | 4168.43  |   | 0        | 51.71 (7.54%)  |
+-------+---------+----------+----------+---+----------+----------------+
| 3     | 56.75   | 4083.84  | 4222.25  |   | 40.83    | 64.7 (6.29%)   |
+-------+---------+----------+----------+---+----------+----------------+
| 4     | 1366.04 | 3762.41  | 5277.69  |   | 74.62    | 86.35 (5.93%)  |
+-------+---------+----------+----------+---+----------+----------------+
| 5     | 1045.79 | 5126.82  | 6321.85  |   | 74.62    | 130.51 (6.64%) |
+-------+---------+----------+----------+---+----------+----------------+
| 6     | 941.11  | 5301.54  | 6391.89  |   | 74.62    | 200.55 (8.1%)  |
+-------+---------+----------+----------+---+----------+----------------+
| 7     | 983.16  | 6307.80  | 7459.64  |   | 84.34    | 258.58 (8.43%) |
+-------+---------+----------+----------+---+----------+----------------+
| 8     | 2434.55 | 6819.60  | 9540.31  |   | 143.08   | 280.51 (7.33%) |
+-------+---------+----------+----------+---+----------+----------------+
| 9     | 1924.65 | 7303.06  | 9633.87  |   | 203.08   | 314.07 (6.85%) |
+-------+---------+----------+----------+---+----------+----------------+
| 10    | 35.67   | 13937.29 | 14729.88 |   | 378.46   | 234.7 (4.07%)  |
+-------+---------+----------+----------+---+----------+----------------+
| 11    | 53.46   | 14009.32 | 14876.56 |   | 406.89   | 352.95 (5.09%) |
+-------+---------+----------+----------+---+----------+----------------+